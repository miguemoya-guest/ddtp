#!/bin/sh
#
# $Id$
#
# Copyright (C) 2008, Nicolas François <nicolas.francois@centraliens.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2

type=$1
name=$2

usage () {
	echo "Usage: $0 [ good | bad ] <test_unit_name>" >&2
	exit 1
}

if [ "$#" != "2" ] || [ -z "$name" ]; then
	usage
fi
if [ "$type" != "good" ] && [ "$type" != "bad" ]; then
	usage
fi

cp -a testsuite/good/pristine "testsuite/$type/$name"
find "testsuite/$type/$name" -name ".svn" | xargs rm -rf

ln -s ../good.pkgs/pristine "testsuite/$type.pkgs/$name"

