#!/usr/bin/perl

use diagnostics;
use strict;
use ddts_config;
use ddts_common;


use DBI;
use Digest::MD5 qw(md5_hex);

use MIME::Parser;
use MIME::Entity;
use Text::Iconv;

my $start= shift(@ARGV);

my @DSN = ("DBI:Pg:dbname=ddtp", "", "");

my $dbh = DBI->connect(@DSN,
    { PrintError => 0,
      RaiseError => 1,
      AutoCommit => 0,
    });

die $DBI::errstr unless $dbh;

#
# define constants
#

# regex

my $mMD5  = qr/([a-zA-Z0-9]+)/;
my $mLANG = qr/\b([a-z][a-z](?(?=_)_[A-Z][A-Z]))\b/;
my $mCHAR = qr/(?:\.([a-zA-Z0-9-]+))?/;
my $mPKG  = qr/([\w+.-]+)/;
my $mNB   = qr/(\d+)/;
my $mADDR = qr/([\w+.@-]+)/;
my $mADDRwithUserName = qr/([ "\<\>\(\)\w+\.\@-]+)/;

# Number of days before we ignore the owner when sending descriptions to translate
my $OWNER_EXPIRE = 28;

my $add_translation=0;

sub desc_to_parts ($) {
        my $desc = shift;

        my @parts;
        my $part;
        my @lines=split(/\n/, $desc);

        foreach (@lines) {
                if (not @parts) {
                        push @parts,$_;
                        $part="";
                        next;
                }
                if ($_ ne " .") {
                        $part.=$_;
                        $part.="\n";
                } else {
                        push @parts,$part if ($part ne "");
                        $part="";
                }
        }
        push @parts,$part if ($part ne "");

        return @parts;

}


sub get_one_untran_files_from_db
{
	my $lang_postfix= shift(@_);
	my $save= shift(@_);
	my $from= shift(@_);
	my $description_id= shift(@_); # was my $file= shift(@_);

	my $description_from_db;
	my $description;
	my $headline;
	my $md5sum;
	my @md5sums;
	my @parts;
	my $a;

	my $key;
	my %return;
	my @packagelist;
	my $zeile;
	my $temp_value;
	my $not_update_desc_send=0;

	debug "",
	      "lang_postfix=$lang_postfix",
	      "save=$save",
	      "$description_id";

	@md5sums=();
	@parts=();
	$description="";
	open(SAVE, ">>$save") or suicide "open save_file failed";


	my $prioritize;
	my $package;
	my $source;
	my $active;
	my $owner;
	my $translation;
	my $part;

	my $sth = $dbh->prepare("SELECT A.description,A.prioritize,A.package,A.source,B.description_id,O.owner FROM (description_tb AS A LEFT JOIN active_tb AS B ON A.description_id=B.description_id) LEFT JOIN (SELECT owner,description_id FROM owner_tb WHERE language=?) AS O ON A.description_id=O.description_id WHERE A.description_id=?");
	$sth->execute($lang_postfix,$description_id);

	($description,$prioritize,$package,$source,$active,$owner) = $sth->fetchrow_array ;

	print SAVE "\n"
		."# Source: $source\n"	
		."# Package(s): $package\n"	
		."# Prioritize: $prioritize\n";	
	if ($active) {
		print SAVE "# This Description is active\n";
	}
	if ($owner) {
		print SAVE "# This Description is owned\n";
	}
	print SAVE "Description: $description";	

	@parts=desc_to_parts($description);

	my $num=0;
	foreach (@parts) {
		my $part_md5=md5_hex($_);
		$sth = $dbh->prepare("SELECT part FROM part_tb WHERE part_md5=? and language=?");
		$sth->execute($part_md5,$lang_postfix);

		($part) = $sth->fetchrow_array ;
		if ($num == 0) {
			print SAVE "Description-$lang_postfix: ";
		}
		if ($part) {
			print SAVE "$part";
			if ($num == 0) {
				print SAVE "\n";
			}
		} else {
			if ($num == 0) {
				print SAVE "<trans>\n";
			} else {
				print SAVE " <trans>\n";
			}
		}
		$num+=1;
		if ( ($num>1) and ($num<=$#parts) ) {
			print SAVE " .\n";
		}
	}

	print SAVE "#\n";	
	print SAVE "# other Descriptions of the $package package with a translation in $lang_postfix:\n";	
	print SAVE "# \n";	

	my $description_id2;

	$sth = $dbh->prepare("SELECT A.description_id,B.description_id FROM description_tb AS A LEFT JOIN active_tb AS B ON A.description_id=B.description_id WHERE A.package=? and A.description_id in (SELECT description_id FROM translation_tb WHERE language=?)");
	$sth->execute($package,$lang_postfix);

	while(($description_id2,$active) = $sth->fetchrow_array) {
		if ($description_id2 ne $description_id) {
			print SAVE "# Description-id: $description_id2 http://ddtp.debian.net/ddt.cgi?desc_id=$description_id2\n";	
			print SAVE "# patch http://ddtp.debian.net/ddt.cgi?diff1=$description_id2&diff2=$description_id&language=$lang_postfix\n";	
	
			if ($active) {
				print SAVE "# This Description is active\n";
			}
	
			my $sth2 = $dbh->prepare("SELECT tag,date_begin,date_end FROM description_tag_tb WHERE description_id=?");
			$sth2->execute($description_id2);
		
			my $tag;
			my $date_begin;
			my $date_end;
			while(($tag,$date_begin,$date_end) = $sth2->fetchrow_array) {
				print SAVE "# This Description was in $tag from $date_begin to $date_end;\n";
			}
	        	print SAVE "# \n";
		}
	}

#	if ($md5s -> translation($md5)) {
#		print SAVE "# package is already translated.\n";
#		if (!($md5s -> translator($md5) =~ /\@/)) {
#			print SAVE "# but it has no translator...\n";
#		}
#	}
#
#	$temp_value=get_value_from_db("$db_base/desc_send-$lang_postfix.db","$key");
#	if ($temp_value && (cmp_email_address($from, $temp_value) == EMAILNE)) {
#		print SAVE "# package has already been sent to another translator,\n"
#			  ."# in case you want to review it, please send a mail to the server\n"
#			  ."# with subject: REVIEW ".($md5s -> name($md5))." ".($md5s -> lang)."\n";
#		close SAVE or suicide " db_file failed";
#		return;
#	} elsif ($temp_value =~ /\@/) {
#		$not_update_desc_send=1;
#	}
#	
#	if ($md5s -> translator($md5) && (cmp_email_address($from, $md5s -> translator($md5)) == EMAILNE)) {
#		print SAVE "# package has already been sent to another translator,\n"
#			  ."# in case you want to review it, please send a mail to the server\n"
#			  ."# with subject: REVIEW ".($md5s -> name($md5))." ".($md5s -> lang)."\n";
#		close SAVE or suicide " db_file failed";
#		return;
#	} elsif ($md5s -> translator($md5) =~ /\@/) {
#		$not_update_desc_send=1;
#	}
#	
#	debug "not_update_desc_send = $not_update_desc_send bugs";
#
#	print SAVE addbugs_to_buglist("$key","$lang_postfix");
#
##	my $bugnr;
##	my @bugs=get_bugnr_md5sum("$key","$lang_postfix");
##	debug "found $#bugs bugs";
##	if ($#bugs+1) {
##		print SAVE "# package has open bug(s):\n";
##		print SAVE "#   Please check this bug(s).\n";
##		foreach $bugnr (@bugs) {
##			print SAVE "#    see http://ddtp.debian.org/$server_part/bts/bug$bugnr.txt\n";
##			push (@buglist, $bugnr);
##		}
##	}
#
#	$description_from_db = $md5s -> description($md5);
#	foreach $_ (split(/\n/,$description_from_db)) {
#		$_.="\n";
#		print SAVE $_;
#		if (/^Description.*: /) { # new item
#			($headline) =($_ =~ /^Description.*: (.*)/);
#			$md5sum=md5_hex($headline);
#			push @md5sums, $md5sum;
#			push @parts,   $headline;
#			$description="";
#		}
#		if (/^ \.$/) {
#			#if ($description eq "") {
#			#	debug  "description is empty";
#			#	status "  sorry error in the db: description is empty.!",
#			#	       "    please request untranslated descriptions a second time to get real descriptions.!";
#
#			#	del_value_from_db("$db_base/desc.db","$key");
#			#	del_value_from_db("$db_base/desc_table.db","$key");
#			#	return;
#			#}else {
#				$md5sum=md5_hex($description);
#				push @md5sums, $md5sum;
#				push @parts,   $description;
#				$description="";
#			#}
#			next;
#		}
#		if (/^ /) {
#			$description.=$_;
#		}
#	}
#	if ($description eq "") {
#		# descritiption don't have last (or any) part
#		#  FIXME We should make this better...
#		# I must rewrite this...
#		# 
#		# debug "description is empty";
#		# del_value_from_db("$db_base/desc.db","$key");
#		# del_value_from_db("$db_base/desc_table.db","$key");
#		# status "  sorry error in the db: description is empty.!";
#		#        "    please get more descriptions to get real descriptions.!";
#	}else {
#		$md5sum=md5_hex($description);
#		push @md5sums, $md5sum;
#		push @parts,   $description;
#		$description="";
#	}
#	debug "@md5sums";
#
#	if ($md5s -> translation($md5)) {
#		print SAVE "# translated description from the db:\n" ;
#		print SAVE "# $_\n"	foreach (split(/\n/, $md5s -> translation($md5)));
#		print SAVE "# \n" ;
#	}
#
#	print SAVE "Description-$lang_postfix: " ;
#	print SAVE &get_part_from_db($lang_postfix,$md5sums[0],"<trans>");
#	print SAVE "\n" ;
#	print SAVE &get_part_from_db($lang_postfix,$md5sums[1],&search_ppart_from_db($lang_postfix,$parts[1]," <trans>\n"));
#	#&search_ppart_from_db($lang_postfix,$parts[1],"<trans>\n");
#	if ( $#md5sums >> 1 ) {
#		$a=2;
#		while ($a <= $#md5sums ) {
#			#print "D: $a $md5sums[$a] $parts[$a] $lang_postfix\n";
#			print SAVE " .\n" ;
#			print SAVE &get_part_from_db($lang_postfix,$md5sums[$a],&search_ppart_from_db($lang_postfix,$parts[$a]," <trans>\n"));
#			#&search_ppart_from_db($lang_postfix,$parts[$a],"<trans>\n");
#			$a++
#		}
#	}
#	print SAVE "\n" ;

	close SAVE or suicide " db_file failed";

#	if ($not_update_desc_send==0) {
#		add_value_to_db("$db_base/desc_send-$lang_postfix.db","$key","$from");
#		my $sec=time();
#		add_value_to_db("$db_base/desc_sendtimestamp-$lang_postfix.db","$key","$sec");
#	}
}


sub get_untrans_files_from_db
{
	my $lang_postfix= shift(@_);
	my $save= shift(@_);
	my $from= shift(@_);
	my @files = @_;;

	my $a;

	debug "",
	      "lang_postfix=$lang_postfix",
	      "@files";

	open(SAVE, ">>$save") or suicide "open save_file failed";	# touch so the file is created even if there is nothing in it
	print SAVE "" ;							# this prevent suicide on recoding and ensure a reply mail is
	close SAVE or suicide " db_file failed";			# issued

	$a=0;
	while ($a <= $#files ) {
		&get_one_untran_files_from_db($lang_postfix,$save,$from,$files[$a]);
		$a++
	}
	return ($a);
}

sub own_a_description ($$$) {
	my $desc_id = shift;
	my $lang = shift;
	my $owner = shift;

	eval {
		$dbh->do("DELETE FROM owner_tb WHERE description_id = ? AND language = ? AND lastsend < (CURRENT_DATE - ?::int4)", undef,
					$desc_id, $lang, $OWNER_EXPIRE );
		$dbh->do("INSERT INTO owner_tb (description_id,language,owner,lastsend) VALUES (?,?,?,CURRENT_DATE);", undef, $desc_id, $lang, $owner);
		$dbh->commit;   # commit the changes if we get this far
	};
	if ($@) {
		$dbh->rollback; # undo the incomplete changes
		return 1;	# Failed, translation just given to someone else
	}
	return 0;
}

sub get_request_untrans
{
	my $package= shift(@_);;
	my $save= shift(@_);;
	my $lang_postfix= shift(@_);
	my $from= shift(@_);

	my @files;
	my $count= 9;

	my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_id in (SELECT description_id FROM active_tb) and package=?");
	$sth->execute($package);

	my $description_id;

	while(($description_id) = $sth->fetchrow_array) {
		if ($count > 0) { 
			# We check the return value because a parallel email might have just given the description to someone else
			if( own_a_description($description_id,$lang_postfix,$from) == 0 )
			{
				@files=(@files,$description_id);
				$count--; 
			}
		} else {
			last;
		}
	}

	if ($#files >= 0) {
		$count=&get_untrans_files_from_db($lang_postfix,$save,$from,@files);
	} else {
		status "ERROR: The server doesn't find a package",
			      "with this name!";
		$count = 0;
	}

	debug	"count = $count";
	return ($count);
}

sub get_some_untrans
{
	my $count= shift(@_);;
	my $save= shift(@_);;
	my $lang_postfix= shift(@_);
	my $from= shift(@_);
	my $section= shift(@_);

	my @files;

	debug "",
	      "count=$count",
	      "lang_postfix=$lang_postfix";

#	if ($section) {
#		@untrans=select_section("$section",@untrans);
#	}

	my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_id in (SELECT description_id FROM active_tb) and description_id not in (SELECT description_id FROM translation_tb WHERE language=?) and description_id not in (SELECT description_id FROM owner_tb WHERE language=? AND lastsend >= (CURRENT_DATE - ?::int4)) ORDER BY prioritize DESC LIMIT 100");
	$sth->execute($lang_postfix,$lang_postfix, $OWNER_EXPIRE);

	my $description_id;

	while(($description_id) = $sth->fetchrow_array) {
		if ($count > 0) { 
			# We check the return value because a parallel email might have just given the description to someone else
			if( own_a_description($description_id,$lang_postfix,$from) == 0 )
			{
				@files=(@files,$description_id);
				$count--; 
			}
		} else {
			last;
		}
	}

	if ($#files >= 0) {
		$count=&get_untrans_files_from_db($lang_postfix,$save,$from,@files);
	} else {
		status "ERROR: The server doesn't find a package",
			      "with this name!";
	}

	debug	"count = $count";
	return ($count);
}

sub recode_file
{
	my $file = shift (@_);
	my $lang_postfix = shift (@_);
	my $from_charset = shift (@_);
	my $to_charset = shift (@_);

	my $text="";

	open (FILE, "<$file") or suicide "open $file ($!) failed";
	while (<FILE>) {
		$text.= $_;
	}
	close FILE;

	status "transform data from $from_charset to $to_charset...";
	eval {
		my $converter = Text::Iconv->new("$from_charset", "$to_charset");
		$text=  $converter->convert("$text");
	};
	if ($text ne "") {
		$text=~ s/^Description-$lang_postfix:/Description-$lang_postfix.$to_charset:/gm; 
		$text=~ s/^Description-$lang_postfix.$from_charset:/Description-$lang_postfix.$to_charset:/gm; 
	
		open (FILE, ">$file") or suicide "open $file ($!) failed";
		print FILE $text;
		close FILE;
	}
}


sub add_description_to_db
{
	my $from_address= shift(@_);
	my $description= shift(@_);
	my $description_trans= shift(@_);
	my $lang_postfix= shift(@_);
	
	my $md5sum_description;
	my $md5sum_description_trans;
	my $package="";
	my $first_package_char="";
	my $db_item_dir;
	my $db_item_name;
	my $db_item_from;
	my $db_item_new_name;
	my $db_item_new_from;

	my $return_db;
	my $sec;
	my $old_from;
	my @packagelist;

	my $description_id;

	my $not_add_to_db=0;

	debug "",
	      "from_address=$from_address",
#	      "description=$description";
#	      "description_trans=$description_trans";
	      "lang_postfix=$lang_postfix";

	$md5sum_description=md5_hex($description);
	$md5sum_description_trans=md5_hex($description_trans);

	debug "md5sum_description=$md5sum_description";



	sub get_description_id {
		my $md5sum= shift(@_);

		my $description_id;

		my $sth = $dbh->prepare("SELECT description_id FROM description_tb WHERE description_md5=?");
		$sth->execute($md5sum);
		($description_id) = $sth->fetchrow_array;
		return $description_id;
	}

	sub get_translation_id {
		my $description_id= shift(@_);
		my $lang= shift(@_);

		my $translation_id;

		my $sth = $dbh->prepare("SELECT translation_id FROM translation_tb WHERE description_id=? and language=?");
		$sth->execute($description_id,$lang);
		($translation_id) = $sth->fetchrow_array;
		return $translation_id;
	}

	sub get_description {
		my $description_id= shift(@_);

		my $description;

		my $sth = $dbh->prepare("SELECT description FROM description_tb WHERE description_id=?");
		$sth->execute($description_id);
		($description) = $sth->fetchrow_array;
		return $description;
	}

	sub get_part_id {
		my $part_md5= shift(@_);
		my $lang= shift(@_);

		my $part_id;

		my $sth = $dbh->prepare("SELECT part_id FROM part_tb WHERE part_md5=? and language=?");
		$sth->execute($part_md5,$lang);
		($part_id) = $sth->fetchrow_array;
		return $part_id;
	}

	sub save_parts_to_db {
		my $md5sum= shift(@_);
		my $part  = shift(@_);
		my $lang  = shift(@_);

		if ($part and $md5sum) {
			eval {
				my $part_id=get_part_id($md5sum,$lang);
				if ($part_id) {
					$dbh->do("UPDATE part_tb SET part = ? WHERE part_id = ?;", undef, $part, $part_id);
				} else {
					$dbh->do("INSERT INTO part_tb (part_md5, part, language) VALUES (?,?,?);", undef, 
						$md5sum,$part,$lang);
				}
				$dbh->commit;   # commit the changes if we get this far
			};
			if ($@) {
				warn "Transaction aborted because $@";
				$dbh->rollback; # undo the incomplete changes
				print $part . "\n" ;
			}
		}
	}

	my $translation_orig=$description_trans;
	$description_id=get_description_id($md5sum_description);
	if ($description_id) {
		eval {
			my $translation_id=get_translation_id($description_id,$lang_postfix);
			# FIXME:  add update a translation
			#         update only if sender is owner
			#         etc.
			if (not $translation_id) {
				$dbh->do("INSERT INTO translation_tb (description_id, translation, language) VALUES (?,?,?);", undef, $description_id,$description_trans,$lang_postfix);
			} else {
				debug "UPDATE translation_tb SET translation='$description_trans' WHERE description_id='$description_id' and language='$lang_postfix';";
				$dbh->do("UPDATE translation_tb SET translation=? WHERE description_id=? and language=?;", undef,
					$description_trans, $description_id, $lang_postfix);
			}
			$dbh->commit;   # commit the changes if we get this far
		};
		if ($@) {
			warn "Transaction aborted because $@";
			$dbh->rollback; # undo the incomplete changes
		} else {
			status	"add the translation in the db";
			status	"see http://ddtp.debian.net/ddt.cgi?desc_id=$description_id&language=$lang_postfix";
			status	"";
			own_a_description($description_id,$lang_postfix,$from_address);
		}
	} else {
		status	"the package description was not found in the db";
		status	"";
	}

	if ($description_id) {
		my @t_parts = desc_to_parts($translation_orig);
		my @e_parts = desc_to_parts($description);

		my @e_parts_md5;
		foreach (@e_parts) {
			push @e_parts_md5,md5_hex($_);					
		}

		if ($#e_parts_md5 = $#t_parts) {
			my $a=0;
			while ($a <= $#e_parts_md5 ) {
				&save_parts_to_db($e_parts_md5[$a],$t_parts[$a],$lang_postfix);
				#&add_pparts_to_db($parts[$a],$parts_trans[$a],$lang_postfix);
				$a++
			}
		}
	}

#	$return_db = get_value_from_db("$db_base/desc.db",$md5sum_description);
#	if ($return_db) {
#		debug "### Found Package! ### (new desc db)";
#		$old_from  = get_value_from_db("$db_base/desc_from-$lang_postfix.db",$md5sum_description);
#		$return_db = get_value_from_db("$db_base/desc-$lang_postfix.db",$md5sum_description);
#		if ($return_db) {
#			debug  "new desc db: already translated!",
#			       "old_from : $old_from";
#			status "   description is already in db";
#			if (cmp_email_address($old_from, $from_address, CMPEMPTY) == EMAILEQ) {
#				#debug "address are equal";
#				if ($description_trans eq $return_db) {
#					debug  "translation is unchanged";
#					status "     and the translation is unchanged";
#					$not_add_to_db=1;
#					if ((!("$old_from" =~ /\@/)) and (not test_if_reviewer($md5sum_description,$from_address,$lang_postfix))) {
#						debug "but we found a new translator";
#						status "     this description has no translator",
#						       "     and now you are the new translator";
#						add_value_to_db("$db_base/desc_from-$lang_postfix.db","$md5sum_description","$from_address");
#					}
#				} else {
#					add_value_to_db("$db_base/desc_old-$lang_postfix.db","$md5sum_description-$uni_sec","$return_db");
#					add_value_to_db("$db_base/desc_old_from-$lang_postfix.db","$md5sum_description-$uni_sec","$old_from");
#				}
#			} else {
#				$not_add_to_db=1;
#			}
#		};
#		if ($not_add_to_db==0) {
#			# build desc_db
#			if (not test_if_reviewer($md5sum_description,$from_address,$lang_postfix)) {
#				@packagelist=get_packagename_by_md5sum($md5sum_description);
#				status "  adding description to db:  @packagelist";
#				add_value_to_db("$db_base/desc_from-$lang_postfix.db","$md5sum_description","$from_address");
#				add_value_to_db("$db_base/desc-$lang_postfix.db","$md5sum_description","$description_trans");
#				# resend description to reviewers so they review the good one
#				&resend_to_reviewers($md5sum_description, $lang_postfix);
#				#&save_l10n_mails("$lang_postfix","$from_address","$description","$description_trans");
#				&send_notification($md5sum_description, $lang_postfix);
#				&send_PTS_notification($md5sum_description, $lang_postfix);
#				unlink ("$update_html_base/$md5sum_description-$lang_postfix.txt");
#				unlink ("$p_update_html_base/$md5sum_description-$lang_postfix.txt");
#				&bts_new_upload($md5sum_description,$lang_postfix,$description_trans);
#			} else {
#				status "  you are a reviewer of this translation";
#				&set_review_ok($md5sum_description, $from_address, $lang_postfix);	# set flag and test if over
#			}
#		} else {
#			$return_db = get_value_from_db("$db_base/desc-$lang_postfix.db",$md5sum_description);
#			if ($description_trans eq $return_db) {
#				debug  "translation is unchanged";
#				status "     and the translation is unchanged";
#				&set_review_ok($md5sum_description, $from_address, $lang_postfix);	# set flag and test if over
#			} else {
#				status "   and not changed in the db!",
#				       "   because you are not the 'owner'!",
#				       "        the server sent a bug report",
#				       "        to the first translator.";
#				&set_review_buggy($md5sum_description, $from_address, $lang_postfix);	# set flag
#				add_and_mail_bts("$lang_postfix","$md5sum_description","","$description_trans","$from_address");
#			}
#		}
#	} else {
#		status "ERROR: can't find the orignal description in the db!",
#			      "Never change orignal description!";
#	}
}


# scan_attachment
#
# scan other attached files (i.e. descriptions)
#
# input:
#   mime part entity
#   mail address
#
sub scan_attachment ($$) {
	my $part	 = shift;		# mime part
	my $from_address = shift;

	my $description;
	my $description_trans;
	my $description_trans_format;
	my $lang_postfix;
	my $desc_charset;
	my @btsclose_nrs;
	my $btsclose_nr;

	debug	"",
		"attachment_file = ".$part -> head -> recommended_filename,
		"from_address = $from_address";

	if ($part -> head -> recommended_filename) {
		status	"----- start: ".$part -> head -> recommended_filename;
	} else {
		debug	" no file name, skip attachment"; 
		status	"----- skip: attachment without filename"; 
		return;
	}

	$add_translation++;

	$btsclose_nr = 0;

	my @lines = $part -> bodyhandle -> as_lines;
	push @lines, "";

	foreach (@lines) {
		s///;
		chomp;

		next if (/^#(?!# )/);				# comment

		if (/^(?i:from:) $mADDRwithUserName/) {
			debug	"Found FROM:",
				"from = $1";

			$from_address = "$1\n";

			status	"from: $1";

			next;
		}
		if (/^(?i:btsclose:)/){
			my @nb = /(\d+)/g;

			debug	"found BTSCLOSE",
				"btsclose_nr = ".join(" ", @nb);

			push @btsclose_nrs, @nb;

			status	"btsclose: ".join(" ", @nb);

			next;
		}
		if (/^Description: (.*)/) { # new item
			debug	"Found Description-line: $_"; 

			$description = "$1\n";

			status	"Description: $1";

			next;
		}
		if (/^Description-$mLANG$mCHAR\: (.*)/) { # new item
			debug	"lang_postfix = $1",
				"desc_charset = ".($2 || "");

			$description_trans = "$3\n";
			$lang_postfix      = $1;
			$desc_charset      = $2;
#			$glob_lang_postfix = $1;

			if ($lang_postfix eq "pt_PT") {
				$lang_postfix="pt";
			}
			if ($lang_postfix eq "pl_PL") {
				$lang_postfix="pl";
			}
			if ($lang_postfix eq "ru_RU") {
				$lang_postfix="ru";
			}

			status	"Description-$lang_postfix: $3";

			if (length($3) > 80) {
				debug	"translated head line too long!"; 

				status	"WARNING: translated short description too long (>80 characters)",
						"Please edit and resend";
			}

			next;
		}
		if (/^## /) {
#			if ($description_trans) {
#				$description_trans .= "$_\n";
#			} else {
#				status	"ERROR: reviewer comment",
#						"comment out of translation",
#						"comment skipped";
#			}

			next;
		}
		if (/^ \../) {
			debug	"Found ' .X'"; 


			status	"ERROR: _.x",
					"line beginning with space, full stop and another character",
					"This is not allowed! Please, change your description",
					"and resend it to the server!",
					"If the error is in the orignal description,",
					"please write a bug report!)",
					"The line is: $_";
		}
		if (/^\s*$/) {
			debug	"new attachment part",
				"from_address = $from_address",
			#	"description = $description",
			#	"description_trans = $description_trans",
				"lang_postfix = $lang_postfix";

			next unless ($description	||
				     $description_trans	||
				     $from_address	  );

			if (not $desc_charset) {
				$desc_charset = $charset{$lang_postfix} || 'UTF-8';
			}
			if (($desc_charset eq "UTF-8") or ($desc_charset eq "utf-8")) {
				status	"              verifying UTF-8...";
			} else {
				status	"              transform from $desc_charset to UTF-8...";
			}
			eval {
				my $converter = Text::Iconv->new("$desc_charset", "UTF-8");
				$description_trans =  $converter->convert("$description_trans");
				#$description_trans =~ s/^Description-$lang_postfix\.$desc_charset:/Description-$lang_postfix:/g; 
			};
			if( not defined $description_trans )
			{
				status  "              invalid UTF-8!!!";
			}

			if (not ($description	    &&
				 $description_trans &&
				 $from_address	    &&
				 $lang_postfix	      )) {
				debug  ((not $from_address	    ? "   from_address not set!"      : undef),
					(not $description	    ? "   description not set!"	      : undef),
					(not $description_trans     ? "   description_trans not set!" : undef),
					(not $lang_postfix	    ? "   lang_postfix not set!"      : undef));

#				status	"ERROR: parsing",
#					($from_address	    ? "missing sender address"	: undef),
#					($lang_postfix	    ? "missing language"	: undef),
#					($description	    ? "missing description"	: undef),
#					($description_trans ? "missing translation"	: undef);
			} else {
#				$description_trans_format = format_description($description_trans);
				$description_trans_format = $description_trans;
				if ($description_trans_format ne $description_trans) {
					#debug	"reformat description_trans:!",
					#	"description_trans = $description_trans:",
					#	"description_trans_format = $description_trans_format:";
					status	"WARNING: The translation might have long lines.",
							 "I reformatted the translated description",
							 "check the new format of the description:";
					status	"----Begin----",
						$description_trans_format,
						"---- End ----";
					$description_trans = $description_trans_format;
				}
				if ($description_trans =~ m/<trans>/ ) {
					debug	"found a '<trans>' in translated description!";
					status	"WARNING: <trans>",
							 "Maybe this is what you meant, if you couldn't translate the",
							 "description entirely. Otherwise, edit and send it again",
							 "Description not added to db";
				} else {
					# add this...
					&add_description_to_db($from_address, $description, $description_trans, $lang_postfix);
				}
#				&add_parts_from_description_to_db($description, $description_trans, $lang_postfix);
#				debug	"test btsclose_nrs = @btsclose_nrs";
#				foreach $btsclose_nr (@btsclose_nrs) {
#					debug	"test btsclose_nr = $btsclose_nr";
#					if ($btsclose_nr>0) {
#						del_bugnr($btsclose_nr,$from_address);
#						$btsclose_nr = 0;
#					}
#				}
			}
			$description	   = "";
			$description_trans = "";
			$lang_postfix	   = "";

			next;
		}
		if (/^ /) {
			if ($description_trans) {
				$description_trans .= "$_\n";
			} else {
				$description .= "$_\n";
			}

			next;
		}
		if (/^[^ ]/) {
			debug	"Found a wrong line....."; 
			status	"ERROR:  found an incomprehensible line!",
					"The line doesn't start with:",
					"'#'                for a comment",
#					"'## '              for a review comment sent to the translator",
					"'from: '           for another From",
					"'Description: '    for the orig. description",
					"'Description-xx: ' for the trans. description",
					"' '                for a multiline description",
					"The line is: ",
					$_,
					"Check this out. Maybe you forgot the leading space...";
		}
	}

	if ($part -> head -> recommended_filename) {
		status	"----- end: ".$part -> head -> recommended_filename;
	} else {
		status	"----- end: ";
	}
}


# mailparser
#
# parse incomming mail
#
sub mailparser () {
	chdir $basedir;

	my $parser = new MIME::Parser;
	   $parser -> output_under   ("$my_tmpdir");
	   $parser -> output_prefix("td-"       );

	my $mail_in = $parser  -> read(\*STDIN);
	my $head_in = $mail_in -> head;
	   $head_in -> unfold;				# remove internal new line in all fields

	if( $head_in->get("Return-Path") =~ /^<>/ ) {
		warning "Apparently a bounce: " . $head_in->get("Return-Path");
		exit 0;
	}
	if( $head_in->get("X-DDTS-Mail") =~ /^server/ ) {
		warning "DDTS mail: " . $head_in->get("X-DDTS-Mail");
		exit 0;
	}
	my $from       = $head_in -> get('From'      );
	my $reply_to   = $head_in -> get('Reply-To'  );
	my $mailto     = $head_in -> get('To'        );
	my $subject    = $head_in -> get('Subject'   );
	my $messageid  = $head_in -> get('Message-ID');
	my $references = $head_in -> get('References');

	debug	"",
		"from  = $from",
		"MsgID = $messageid",
		($reply_to ? "effec.from = $reply_to\n" : undef);

	$from = $reply_to if $reply_to;

	if (not ($messageid)) {
		suicide "no messageid";
	}

	if ($from =~ /\@(?i:selex-comms.com)/) {
		suicide "from selex-comms.com";
	}
	if ($from =~ /(?i:mailmarshal)\@/) {
		suicide "from mailmarshal";
	}
	if ($from =~ /(?i:imss\@city.uozu.lg.jp)/) {
		suicide "from imss\@city.uozu.lg.jp";
	}
	if ($from =~ /(agl.com.au)/) {
		suicide "from agl.com.au";
	}
	if ($from =~ /(?i:postmaster)\@/) {
		suicide "from postmaster";
	}
	if ($from =~ /(?i:administrator)\@/) {
		suicide "from administrator";
	}
	if ($from =~ /(?i:MAILER-DAEMON)\@/) {
		suicide "from Mailer-Daemon";
	}
	if ($from =~ /\@ddtp\.debian\.net/) {
		suicide "Break the loop!!!",
			"Break the loop!!!",
			"Break the loop!!!",
			"Break the loop!!!";
	}

	suicide "No from!!!" unless $from;

	status	"from $from";

	my $lang_postfix;
	my $desc_charset;
	my $new_file;
#	my $copy_file;
	my $add_guide=0;
#	my $add_status=0;
	my $count=0;
	my $package;
	my $section;
	my $file;
#	my @untranslated;

	$_ = $subject;
	s/\s+/ /g;

	debug "subject = $_";

	if (/\b(?i:lang)\b/) {
	    /\b(?i:lang) $mLANG$mCHAR/;
		debug	"found LANG",
			"lang_postfix = ".($1 || ""),
			"desc_charset = ".($2 || "");

		if ($1) {
			$lang_postfix = $1;
			$desc_charset = $2;

			status	"lang $1".($2 ? ".$2" :
						""     );
		} else {
			$lang_postfix = "";
			$count	      = 0;

			status	"ERROR: lang",
					"missing lang_postfix";
		}

		if ($lang_postfix eq "pt_PT") {
			$lang_postfix="pt";
		}
		if ($lang_postfix eq "pl_PL") {
			$lang_postfix="pl";
		}
		if ($lang_postfix eq "ru_RU") {
			$lang_postfix="ru";
		}
	}

	#
	# parse attached files
	#
	foreach my $part ($mail_in -> parts) {
		$file = $my_tmpdir."/".$part -> head -> recommended_filename;

#		if ($file =~ /\W(?i:ddts[_-]commands?\.txt)$/) {		# Found a ddts_commands file
#			debug	"command attachment #$cmd_flag";
#
#			if ($cmd_flag > 1) {
#				debug	"ignored";
#
#				$cmd_flag++;
#
#				status	"ERROR: ddts-commands.txt",
#						"only one command attachment allowed",
#						"file NOT parsed";
#				next;
#			} elsif (not $cmd_flag) {
#				debug	"ignored";
#
#				status	"ERROR: ddts-commands.txt",
#						"file found but missing 'command <language>' subject",
#						"file NOT parsed";
#				next;
#			}
#			
#			$file_get    = "$my_tmpdir/new.ddtp";			# file for get    commands
#			$file_review = "$my_tmpdir/newreview";			# file for review commands
#			unlink $file_get;
#			unlink $file_review;
#			debug "file = $my_tmpdir/$file";
#
#			($from, $cmd_get, $cmd_review, $add_guide, $add_status) =
#				&scan_ddts_commands($part, $lang_postfix, $from, $file_get, $file_review, $my_tmpdir);
#			&recode_file($file_get,    $lang_postfix, $charset{$lang_postfix}, $desc_charset) if $desc_charset;
#			&recode_file($file_review, $lang_postfix, $charset{$lang_postfix}, $desc_charset) if $desc_charset;
#
#			$cmd_flag++;
#
#			next;							# don't scan as normal attachment
#		}
		if ($file =~ m/\Wwinmail\.dat$/) { 
			status	"ERROR: winmail.dat",
					"tnef attachement not allowed, use normal MIME attachement!",
					"file NOT parsed";
			next;
		};
#		if ($file =~ m/\Wddt[sp]_list\.txt$/) { 
#			@untranslated = &scan_ddts_list($part, $lang_postfix, $from);
#			next;
#		};
		next if $file =~ m/\Wstatus_text$/;
		next if $file =~ m/\Wlog\.txt$/;
		next if $file =~ 	    m/\WHOT-NEWs(-..(_..)?)?\.txt$/;	####################
		next if $file =~ 		 m/\Wbts(-..(_..)?)?\.txt$/;	#
		next if $file =~	     m/\Wchanged(-..(_..)?)?\.txt$/;	#  should find something better here
		next if $file =~ m/\Wresend_to_reviewers(-..(_..)?)?\.txt$/;	#
		next if $file =~ 	       m/\Wguide(-..(_..)?)?\.txt$/;	####################

		scan_attachment($part, $from);
	}

	$_ = $subject;
	s/\s+/ /g;

	if (/\b(?i:nothing)\b/) {
		debug	"found NOTHING";

		$add_guide = 0;

		status	"nothing";
	} elsif	(/\b(?i:command)\b/) {
	} else {
		if (/\b(?i:help)\b/) {
			debug	"found HELP";

			$add_guide = 1;
		};
		if (/\b(?i:guide)\b/) {
			debug	"found GUIDE";

			$add_guide = 1;
		};
		if (/\b(?i:noguide)\b/) {
			debug	"found NOGUIDE";

			$add_guide = 0;
		};
		if (/\b(?i:get)\b/) {
			 /\b(?i:get) (?:$mNB|$mPKG) $mLANG$mCHAR/;
			debug	"found GET",
				"number = "	 .($1 || ""),
				"package = "	 .($2 || ""),
				"lang_postfix = ".($3 || ""),
				"desc_charset = ".($4 || "");

			if	(not ($1 || $2)) {
				$lang_postfix = "";
				$count	      = 0;

				status	"ERROR: get",
						"missing number or package name";
			} elsif (not $3) {
				$lang_postfix = "";
				$count	      = 0;

				status	"ERROR: get",
						"missing language";
			} else {
				if ($2) {
					$package = $2;
					$count  = 1;
				} else {
					$count  = ($1 > 9 ? 9 : $1);
				}
				$lang_postfix = $3;
				$desc_charset = $4;

		                if ($lang_postfix eq "pt_PT") {
			                $lang_postfix="pt";
		                }
		                if ($lang_postfix eq "pl_PL") {
			                $lang_postfix="pl";
		                }
		                if ($lang_postfix eq "ru_RU") {
			                $lang_postfix="ru";
		                }

				$new_file = "$my_tmpdir/new.ddtp";	# create new filename
				unlink $new_file;			# delete it if it already exists
				if ($2) {
					$count = &get_request_untrans($package, $new_file, $lang_postfix, $from);
				} else {
					$count = &get_some_untrans($count,  $new_file, $lang_postfix, $from, $section);
				}
				if( $count ) {
					&recode_file($new_file, $lang_postfix, "UTF-8", $desc_charset) if $desc_charset;
					&recode_file($new_file, $lang_postfix, "UTF-8", $charset{$lang_postfix} || "UTF-8" ) if not $desc_charset;
				}

				status	"get ".($package || $count)." $3".($4 ? ".$4" : ""     );
			}
		}
	};

	debug	"add_guide = $add_guide";

#	unless ($lang_postfix =~ /^$mLANG$/o) {
#		debug	"set lang_postfix from glob_lang_postfix",
#			"lang_postfix = $lang_postfix";
#
#		$lang_postfix = $glob_lang_postfix;
#	}


#	status "add bugreport $_" foreach (@buglist);
#	status "----- end: log";

	#
	# Create mail answer
	#
	my $server_from_address = 'pdesc@ddtp.debian.net';
	my $mail_out = build MIME::Entity::				# build mail header
			Type		=> "multipart/mixed",
			From		=> $server_from_address,
			Sender		=> $server_from_address,
			To		=> $from,
			Bcc		=> "$log_address{$lang_postfix}, $server_log_address",
			Subject		=> "nothing",
			References	=> "$references $messageid",
			"In-Reply-To"	=> $messageid,
			"X-DDTS-Mail"	=> "server reply";
	if ($desc_charset) {
		$mail_out -> head -> replace ('Subject', "GET "   . $count." $lang_postfix." . $desc_charset . " noguide") if $count>0;
		$mail_out -> head -> replace ('Subject', "REVIEW ".-$count." $lang_postfix." . $desc_charset . " noguide") if $count<0;
	} elsif ($charset{$lang_postfix}) {
		$mail_out -> head -> replace ('Subject', "GET "   . $count." $lang_postfix." . $charset{$lang_postfix} . " noguide") if $count>0;
		$mail_out -> head -> replace ('Subject', "REVIEW ".-$count." $lang_postfix." . $charset{$lang_postfix} . " noguide") if $count<0;
	} else {
		$mail_out -> head -> replace ('Subject', "GET "   . $count." $lang_postfix." . "UTF-8" . " noguide") if $count>0;
		$mail_out -> head -> replace ('Subject', "REVIEW ".-$count." $lang_postfix." . "UTF-8" . " noguide") if $count<0;
	}

	if (-R "$hotnewsdir/HOT-NEWs-$lang_postfix.txt") {		# attach language hot news
		attach $mail_out
			Path		=> "$hotnewsdir/HOT-NEWs-$lang_postfix.txt",
			Charset		=> $charset{$lang_postfix},
			Encoding	=> "quoted-printable";
	}
	if (-R "$hotnewsdir/HOT-NEWs.txt") {				# attach hot news
		attach $mail_out
			Path		=> "$hotnewsdir/HOT-NEWs.txt",
			Charset		=> "ISO-8859-1",
			Encoding	=> "quoted-printable";
	}

	if ($add_guide) {				# attach hot news
		attach $mail_out
			Path		=> "$guidesdir/guide.txt",
			Charset		=> "ISO-8859-1",
			Encoding	=> "quoted-printable";
	}

#	attach_translated ($mail_out,					# attach guide
#			  "$guidesdir/guide.txt",
#			   $lang_postfix) if $add_guide;

	attach $mail_out						# attach log
			Data		=> $status_text,
			Charset		=> "ISO-8859-1",
			Encoding	=> "quoted-printable",
			Filename	=> "log.txt";

#	if ($add_status) {						# attach status
#		attach $mail_out
#			Path		=> "$statusdir/status-$lang_postfix.txt",
#			Charset		=> "ISO-8859-1",
#			Encoding	=> "quoted-printable";
#	}
	debug	"count = $count";
	if ($count && (-e $new_file)) {				# attach new descriptions to translate/review
		attach $mail_out
			Path		=> "$new_file",
			Type		=> "application/debian-dt",
			Encoding	=> "base64";
	}
#	if (($cmd_get > 0) && (-e $file_get)) {				# attach new descriptions to translate
#		attach $mail_out
#			Path		=> "$file_get",
#			Type		=> "application/debian-dt",
#			Encoding	=> "base64";
#	}
#	if (($cmd_review > 0) && (-e $file_review)) {			# attach new descriptions to review
#		attach $mail_out
#			Path		=> "$file_review",
#			Type		=> "application/debian-dt",
#			Encoding	=> "base64";
#	}
#	foreach (@buglist) {						# attach bugs
#		next unless -e "$bts_html_base/bug$_.txt";
#		attach $mail_out
#			Path		=> "$bts_html_base/bug$_.txt",
#			Type		=> "application/debian-dt",
#			Charset		=> $charset{$lang_postfix},
#			Encoding	=> "base64";
#	}


#	#
#	# Log received mail
#	#
#	$head_in -> add     ('Old-To',	        $_) foreach ($head_in -> get_all ('To'         ));
#	$head_in -> add     ('Old-X-DDTS-Mail', $_) foreach ($head_in -> get_all ('X-DDTS-Mail'));
#	$head_in -> delete  ('To'	  );
#	$head_in -> delete  ('Cc'	  );
#	$head_in -> delete  ('X-DDTS-Mail');
#	$head_in -> replace ('To'         , $log_address{$lang_postfix});
#	$head_in -> replace ('Bcc'        , $server_log_address        );
#	$head_in -> replace ('X-DDTS-Mail', 'income'		       );
#
#	$mail_in  -> smtpsend if mailable($log_address{$lang_postfix});

#	$mail_out -> smtpsend if mailable($from			     );

	debug	"add_translation = $add_translation";
	if (($count<0) || ($count>0) || ($add_guide) || ($add_translation>0)) {
		$mail_out -> smtpsend;
		debug "send mail";
	} else {
		debug "don't send mail";
	}

	$mail_in  -> purge;		# purge, this is only received parts
	#$mail_out -> purge;		# don't purge. This remove files like status-XX, guide-XX.txt, ... 
}


my $my_tmpdir="$tmpdir/$$";
mkdir $my_tmpdir,0700;
unlink <$my_tmpdir/*>;

debug "parser start";

mailparser ();

debug "parser end without error";
