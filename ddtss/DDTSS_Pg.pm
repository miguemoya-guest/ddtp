#use strict;
use POSIX qw(strftime);
#use DB_File::Lock;
use Pg_BDB;
use Data::Dumper;
use Text::Iconv;
use Algorithm::Diff::XS qw(compact_diff);

use ddts_lib;

my $COUNT = 0;

# Open DB for write mode
sub DDTSS_Open_Write
{
  my $db = Pg_BDB->open_write();
  return $db;
}

# Open DB for read mode
sub DDTSS_Open_Read
{
  my $db = Pg_BDB->open_read();
  return $db;
}

# close DB, undef caller reference
sub DDTSS_Close
{
  $_[0]->close();
  undef $_[0];
}

sub ddtss_match
{
  my($db, $prefix, $sub, $regex) = @_;

#  print "ddtss_match: $prefix\n";
  
  $db->search($prefix, $sub, $regex);
}
  
# Get list of suggestions packages
sub DDTSS_Get_Suggestions
{
  my $db = shift;
  my $lang = shift;
  
  my @suggestions;
  
  @suggestions=get_suggestions($lang);
  # $id, $package, $md5, $importer, $importtime
  return [ map { 
      my $refuse = "";
      $db->get("suggestion/$_->[0]/refuse", $refuse);
      [ $_->[0], $_->[1], $_->[2], $_->[3], $_->[4], $refuse ] } sort { $b->[0] <=> $a->[0] } @suggestions ];
  #return [@suggestions];
}

# Get list of suggestions packages
sub DDTSS_Get_Suggestion_Infos
{
  my $db = shift;
  my $suggestion_id = shift;

  my %suggestion=get_suggestion_infos($suggestion_id);

  my $refuse = "";
  $db->get("suggestion/$suggestion_id/refuse", $refuse);

  $suggestion{'refuse'}=$refuse;
  
  return %suggestion;
}

# Get list of untranslated packages
sub DDTSS_Get_Untranslated
{
  my $db = shift;
  my $lang = shift;
  my $user = shift;
  
  my ($key,$value);
  my @todo;
  
  ddtss_match( $db, "$lang/packages/", sub {
    my ($key,$value) = @_;
    return unless $key =~ m,^$lang/packages/([\w.+-]+)$,;
    my $package = $1;
    return unless $value =~ /^untranslated,(\d+)/;
    my $prio = $1;
    
    my $lock = DDTSS_IsLocked( $db, "$lang/packages/$package", $user );
    push @todo, [$prio, $package, $lock];
  }, "^$lang/packages/([\\w.+-]+)\$" );
  return [ map { [ $_->[1], $_->[0], $_->[2] ] } sort { $b->[0] <=> $a->[0] } @todo ];
}

# Get list of untranslated packages
sub DDTSS_Get_Users
{
  my $db = shift;
  
  my ($key,$value);
  my @todo;
  
  ddtss_match( $db, "aliases/", sub {
    my ($key,$value) = @_;
    push @todo, [$key, $value];
  }, "^aliases/" );
  return [ map { [ $_->[0], $_->[1] ] } sort { $b->[0] <=> $a->[0] } @todo ];
}

# Get list of untranslated packages
sub DDTSS_Get_PartTrans
{
  my $db = shift;
  my $milestone = shift;
  
  my @packages=get_parttrans($milestone);
  
  return (@packages);
}

# Get list of packages for review
sub DDTSS_Get_ForReview
{
  my $db = shift;
  my $lang = shift;
  my $user = shift;
  
  my @todo;
  ddtss_match( $db, "$lang/packages/", sub {
    return unless $_[0] =~ m,^$lang/packages/([\w.+-]+)$,;
    my $package = $1;
    return unless $_[1] =~ /^forreview/;

    my $str = " (needs review)";
    my $owner;
    $db->get("$lang/packages/$package/owner",$owner);
    my $timestamp;
    $db->get("$lang/packages/$package/timestamp",$timestamp);
     
    my $reviewers;
    my @r;
    if( $db->get("$lang/packages/$package/reviewers", $reviewers) == 0 )
    {
      @r = split /,/, $reviewers;
    }
    my $c = scalar(@r);
    my $reviewable = 0;
    
    if( $owner eq $user )
    { $str = " (owner, had $c)" }
    else
    {
      if( $c > 0 )
      {
        if( scalar( grep { $_ eq $user } @r ) )
        { 
          $str = " (reviewed, had $c)" 
        }
        else
        {
          $str = " (needs review, had $c)";
          $reviewable = 1;
        }
      }
      else
      {
        $str = " (needs initial review)";
        $reviewable = 1;
      }
    }
    push @todo, [$package,$str,$reviewable,$timestamp];
    
  }, "^$lang/packages/([\\w.+-]+)\$" );
  return [ sort { $a->[3] cmp $b->[3] } @todo ];
}

# Given a package name, find the key in the database that represents the untranslated description
sub DDTSS_Untranslated_Get_Key
{
  my $db = shift;
  my $lang = shift;
  my $package = shift;

  return "$lang/packages/$package/data";
}

# Does a sort of soft-lock on a key
sub DDTSS_TryLock
{
  my $db = shift;
  my $key = shift;
  my $user = shift;

  if( DDTSS_IsLocked( $db,$key,$user ) )
  { return 1; }

  # May update the time
  $db->put( "lock/$key", $user.",".(time()+900) );
  return 0;
}

# Returns true if locked and not by you
sub DDTSS_IsLocked
{
  my $db = shift;
  my $key = shift;
  my $user = shift;
  
  my $field;  
  my($lockuser,$locktime);
  if( $db->get( "lock/$key", $field ) )
  { $db->del( "lock/$key" ); $lockuser = ""; $locktime = 0 }
  else
  {
#    print STDERR "field=$field\n";
    ($lockuser,$locktime) = split /,/, $field;
    # Hack to fix breakage by reparenting, but good check anyway
    if( $locktime !~ /^\d+$/ )
    { return 0 }
  }
  
  if( ($locktime > time()) and ($lockuser ne $user) )
  {
    return 1;
  }
  return 0;
}

# Release lock
sub DDTSS_Unlock
{
  my $db = shift;
  my $key = shift;
  my $user = shift;
  
  my $field;
  if( $db->get( "lock/$key", $field ) == 0 )
  {
    my ($lockuser,$locktime) = split /,/, $field;
    if( $lockuser eq $user )
    {
      $db->del( "lock/$key" );
    }
  }
}

# When a new description comes in, we delete the oldest request
sub DDTSS_Kill_Oldest_Request
{
  my $db = shift;
  my $lang = shift;
  my $oldest = 2**32;

  my($key,$value);
  
  ddtss_match($db, "$lang/requests/", sub
  {
    # Found a sent request
    if( $key =~ m,^$lang/requests/, )
    {
      if( $value < $oldest )
      { $oldest = $value }
    }
  });
  
  $db->del( "$lang/requests/$oldest" );
  return;
}

sub DDTSS_Load_Wordlist
{
  my $lang = shift;
  my $fh;
  open $fh, "/org/ddtp.debian.net/ddtss/words-$lang.txt" or return undef;
  
  my $wordlist = {};
  
  sub get_variations
  {
    my @a;
    my $word = lc shift;
    
    return $word if $word !~ /[\s-]/;

    my $a = $word;
    if( $a =~ s/[\s-]+/-/g )
    { push @a, $a }
    $a = $word;
    if( $a =~ s/[\s-]+/ /g )
    { push @a, $a }
    $a = $word;
    if( $a =~ s/[\s-]+//g )
    { push @a, $a }
    
    return @a;
  }
  
  while(<$fh>)
  {
    chomp;
    my @f = split / *\t */, $_, 2;
    next unless $f[0] =~ /\w/;
    
    my @words = get_variations($f[0]);
    
    for my $word (@words)
    {
      -t STDERR and defined $wordlist->{$word} and print "Duplicate word: $word\n";
      
      $wordlist->{$word} = $f[1];
    }
  }
  
  return $wordlist;
}

sub DDTSS_Get_Team
{
  my $db = shift;
  my $lang = shift;

  my @team;  
  ddtss_match( $db, "aliases/", sub {
    my ($key, $value) = @_;
    return if $value !~ m,^$lang$,;
    return if $key !~ m,aliases/(.*)/lastlanguage$,;
    my $user = $1;
    my $trans = 0;
    $db->get("aliases/$user/counttranslations", $trans);
    my $review = 0;
    $db->get("aliases/$user/countreviews", $review);

    push @team, [ $user, $trans, $review ];
  } );

  return \@team;  
}

sub DDTSS_Get_Translated
{
  my $db = shift;
  my $lang = shift;

  my @trans;  
  ddtss_match( $db, "$lang/logs/", sub {
    my($key,$value) = @_;
    my $state = ($value =~ /add the translation in the db/) ? "ok" : "check";
    return unless $key =~ m,$lang/logs/(\d+)/([\w.+-]+)$,;
    push @trans, [ $2, $state, strftime( "%a %b %e %H:%M:%S %Y", localtime $1), $key ];
  }, "^$lang/logs/(\\d+)/([\\w.+-]+)\$" );
  
  # Should be ordered by time ascending
  @trans = reverse @trans;   # Now descending
  
  if( scalar(@trans) > 20 )
  { splice @trans, 20 }   # Keep only the last 10

#  use Data::Dumper;
#  print Dumper(\@trans);
  return \@trans;  
}


# Looks for Description-<lang>.<encoding>: and recodes that to utf8
sub DDTSS_Recode
{
  my $data = shift;  
  
  my $regex = qr/^Description-(\w+)\.([\w.-]+): (.*)\n(( .*\n)+)/m;
  unless( $data =~ /$regex/ )
  {
    return $data;
  }
  
  my $lang = $1;
  my $enc = $2;
  my $short = $3;
  my $long = $4;
  
  eval {
    my $conv = new Text::Iconv($enc,"UTF-8");
    if( defined $conv )
    {
      if( defined $conv->convert($short) )
      { $short = $conv->convert($short) }
      if( defined $conv->convert($long) )
      { $long = $conv->convert($long) }
    }
  };  
  if($@)
  {
    print STDERR "DDTSS_Recode error: $@\n";
  }
  $data =~ s/$regex/Description-$lang: $short\n$long/;
  
  return $data;
}

sub DDTSS_Check_Valid_Language
{
  my $db = shift;
  my $lang = shift;
  
  my $langs;
  $db->get("langs", $langs);
  my @langs = split /,/,$langs;
  
  if( scalar( grep { $_ eq $lang } @langs ) )
  { return 1 }
  return 0;
}

sub DDTSS_CreateDiff
{
  my $str1 = shift;
  my $str2 = shift;
  my $words = shift;
  
  my(@str1,@str2);
  
  if( $words )
  {
    @str1 = grep { defined and length } split /\b|(\s)/, $str1;
    @str2 = grep { defined and length } split /\b|(\s)/, $str2;
  }
  else
  {
    @str1 = grep { defined and length } split /\b|(\s)|(?=[\xC0-\xF7])/, $str1;
    @str2 = grep { defined and length } split /\b|(\s)|(?=[\xC0-\xF7])/, $str2;
  }
#  if (-t) { print Dumper([\@str1, \@str2, scalar(@str1), scalar(@str2)]) }
  my $diff = compact_diff( \@str1, \@str2 );
#  if (-t) { print Dumper([$diff]) }

  my $changed = 0;
  my $count = scalar(@$diff)/2 - 2;

  my @res3;

#  print $count;
#  print join(",",@$diff), "\n";
  
  for my $c (0..$count)
  {
    if( not $changed )
    {
#      print @str1[ $diff->[2*$c] .. $diff->[2*$c+2] - 1 ];
      push @res3, [ '', join("", @str1[ $diff->[2*$c] .. $diff->[2*$c+2] - 1 ]) ];
    }
    else
    {   
      if( $diff->[2*$c] != $diff->[2*$c+2] )
      {
#        print "<-", @str1[ $diff->[2*$c] .. $diff->[2*$c+2] - 1 ], ">";
        push @res3, [ '-', join("", @str1[ $diff->[2*$c] .. $diff->[2*$c+2] - 1 ]) ];
      }
      if( $diff->[2*$c+1] != $diff->[2*$c+3] )
      {
#        print "<+", @str2[ $diff->[2*$c+1] .. $diff->[2*$c+3] - 1 ], ">";
        push @res3, [ '+', join("", @str2[ $diff->[2*$c+1] .. $diff->[2*$c+3] - 1 ]) ];
      }
    }  
    $changed = not $changed;
  }
  return \@res3;
}

sub DDTSS_GetStats
{
  my $db = shift;
  my (%score,%score2);

  ddtss_match( $db, "", sub
    {
      my($key,$value) = @_;
      if( $key =~ m,^(\w+)/done/([\w+.-]+)$, )
      {
        $score{$1}{done}{$2} = 1;
      }
      if( $key =~ m,^(\w+)/packages/([\w+.-]+)$, )
      {
        my $lang = $1;
        my $package = $2;
        if( $value =~ /^untranslated/ )
        {
          $score{$lang}{untranslated}{$package} = 1;
        }
        else
        {
          $score{$lang}{forreview}{$package} = 1;
        }
      }
    #  print "Key: $key: $hash{$key}\n";
    }, "^(\\w+)/(done|packages)/([\\w+.-]+)\$" );

  for my $lang (keys %score)
  {
    $score2{$lang}{done} = 0;
    $score2{$lang}{untranslated} = 0;
    $score2{$lang}{forreview} = 0;
    for my $type (keys %{ $score{$lang} })
    {
      for my $package (keys %{ $score{$lang}{$type} })
      {
        $score2{$lang}{$type} ++;
      }
    }
  }
  return \%score2;
}

sub DDTSS_Reparent_Owners
{
  my ($db, $oldid, $newid) = @_;
  
  my @todo;
  ddtss_match( $db, "", sub
    {
      my($key,$value) = @_;
      return unless $key =~ m,^\w+/packages/[\w.+-]+/(owner|reviewers)$,;
# Disable reparenting locks, because the algorithm doesn't preverse order
#                  or $key =~ m,^lock/\w+/packages/[\w.+-]+$,);
      return unless $value =~ /$oldid/;
      
      my @r = split /,/,$value;
      my %h = map { ($_ eq $oldid)?($newid => 1):($_ => 1) } @r;
      
      my $newvalue = join(",", keys %h);
      
      -t STDERR and print STDERR "Reparent[$key]: $value => $newvalue\n";
      
      push @todo, [$key, $newvalue];
    }, "^\\w+/packages/[\\w.+-]+/(owner|reviewers)\$" );
    
  for my $task (@todo)
  {
    my($key,$value) = @$task;
    $db->put($key,$value);
  }
    
  return;
}

sub DDTSS_Log
{
  my($db, $key, $data) = @_;
  
  my $log = "";
  $db->get( $key, $log );
  
  $log .= $data."\n";
  
  $db->put( $key, $log );
  
  return;
}

###################################################
## Processing stuff

# Check the number of untranslated messages and if below the minimum, send off a request for a new one
sub process_incoming
{
  my $db = DDTSS_Open_Read();
  
  my $langs;
  $db->get("langs", $langs);
  
  my @langs = split /,/,$langs;
  my( %queue, %min );
  
  for my $lang (@langs)
  {
    $min{$lang} = 0;
    $db->get("$lang/config/minuntranslated", $min{$lang});
    
    $queue{$lang} = 0;
    my ($key, $value);

    ddtss_match( $db, "$lang/", sub {
      my ($key, $value) = @_;
      # Found untranslated data
      if( $key =~ m,^$lang/packages/([\w.+-]+), )
      {
        my $package = $1;
        if( $value =~ /^untranslated/ )
        { 
          if( DDTSS_IsLocked( $db, "$lang/packages/$package", " " ) )
          {
            $queue{$lang} += 0.25;
          }
          else
          {
            $queue{$lang}++;
          }
        }
      }
      # Found a sent request
      if( $key =~ m,^$lang/requests/, )
      {
        if( $value > (time() - 1800) )  # Request sent in the last half hour
        {
          $queue{$lang}++;
        }
      }
    } );
    
  }
  
  my $from;
  $db->get("config/clientemail",$from);
  DDTSS_Close($db);

  if( -t STDERR )
  {
    for my $lang (@langs)
    {
      print STDERR "$lang: $queue{$lang}/$min{$lang}\n";
    }
  }
#  use Data::Dumper;
#  print "process_incoming: ", Dumper(\%queue), "\n";
  
  for my $lang (@langs)
  {
    while( $min{$lang} > $queue{$lang} )
    {
      get_untrans( $lang, $from );
      $queue{$lang}++;
    }
  }
}

sub submit_one_suggestion
{
  my($package,$version,$description_md5,$translation,$language,$importer);

  $package =  shift(@_);
  $version =  shift(@_);
  $description_md5 =  shift(@_);
  $translation =  shift(@_);
  $language =  shift(@_);
  $importer =  shift(@_);

  return add_suggestion_to_db($package,$version,$description_md5,$translation,$language,$importer);

}

sub submit_one_package
{
  my($lang,$package);

  $lang =  shift(@_);
  $package =  shift(@_);

  my $db = DDTSS_Open_Write();

  # Get details of translation
  my( $short,$long,$data,$trans,$owner,$reviewers,$owner_email );
  $db->get("$lang/packages/$package/short", $short );
  $db->get("$lang/packages/$package/long", $long );
  $db->get("$lang/packages/$package/data", $data );
  $db->get("$lang/packages/$package/owner", $owner );
  $db->get("$lang/packages/$package/reviewers", $reviewers );
  if( $db->get("aliases/$owner", $owner_email ) ) 
  {
    $db->get("config/clientemail",$owner_email);
  }
  
  $reviewers ||= '';

  # Replace any middle dots with non-breaking spaces. Workaround for buggy browsers
  $long =~ s/\xC2\xB7/\xC2\xA0/g; 
  $short =~ s/\xC2\xB7/\xC2\xA0/g;
    
  # Substitute translated description in, for logs
  unless( $data =~ s/^Description-[\w.-]+:(( .*)?\n)+#/Description-$lang.UTF-8: $short\n$long\n#/m )
  {
    print STDERR "Substitution failed for $package (lang $lang), not sending ($data)\n";
    DDTSS_Close($db);
    return;
  }

  # Extract description for submitting to db
  my $description = $data;
  unless( $description =~ s/.*^Description: ((.*?)\n.*)^Description-.*/$1/ms )
  {
    print STDERR "Substitution failed for $package (lang $lang), not sending ($description)\n";
    DDTSS_Close($db);
    return;
  }
  my $newshort = $2;
  $trans = "$short\n$long\n";

  # Add our data
  eval {
    $status_text = "";
    if (add_description_to_db($owner_email, $description, $trans, $lang) eq -1) 
    { die "failed\n" ;}

  };
  $status_text = "Directly inserted into DB\n\n----- start:\n".
                 "Description: $newshort\n".
                 "Description-$lang: $short\n".
                 $status_text."\n$@----- end:\n";
  if( not $@ )
  {
    $db->put( "todo/in-log/$$.".time().".$COUNT", $status_text );
    $COUNT++;
    $data = "# Creator: $owner\n# Reviewers: $reviewers\n".$data;
    -t STDERR and print STDERR "Sent updated description for $package ($lang)\n";
    # If worked, store result
    $db->put("$lang/done/$package", $data);
    $db->del("$lang/packages/$package");
    $db->del("$lang/packages/$package/short");
    $db->del("$lang/packages/$package/owner");
    $db->del("$lang/packages/$package/long");
    $db->del("$lang/packages/$package/data");
    $db->del("$lang/packages/$package/age");
    $db->del("$lang/packages/$package/reviewers");
    $db->del("$lang/packages/$package/timestamp");
    $db->del("$lang/packages/$package/oldshort");
    $db->del("$lang/packages/$package/oldlong");
    $db->del("$lang/packages/$package/iter");
    $db->del("$lang/packages/$package/comment");
    $db->del("$lang/packages/$package/log");
  }                                                             
  else
  {
    $db->put( "todo/in-log/$$.".time().".$COUNT", $status_text );
    $COUNT++;
    print STDERR "add to db failed ($@)\n";
  }

  DDTSS_Close($db);
  
  # Process the above generated TODO
  process_todo();
}
    
#    print "===\n$data\n===\n";
# Go through the reviewed translations and send them if they've got enough reviews
sub process_reviewed
{
  my $db = DDTSS_Open_Read();

  my ($key, $value, $langs);
  my (@todo,@pending);

  $langs = shift;
  if( not defined $langs )
  {
    $db->get("langs", $langs);
  }
  
  my @langs = split /,/,$langs;
  
  for my $lang (@langs)
  {
    my $numreviewers;
    $db->get("$lang/config/numreviewers", $numreviewers );

    ddtss_match( $db, "$lang/packages/", sub {
      my ($key, $value) = @_;
      return unless $key =~ m,^$lang/packages/([\w.+-]+)$,;
      my $package = $1;
      return unless $value =~ /^forreview/;

      if( $numreviewers > 0 )
      {
        my $reviewers;
        if( $db->get("$lang/packages/$package/reviewers",$reviewers) == 0 )
        {
          my @a;             # Dummy for split
          my $timestamp = 0; # timestamp of package
          my $t = 0;         # Time delay bonus
          my $r = scalar( @a=split /,/,$reviewers );  # Number of reviewers
          
          $db->get("$lang/packages/$package/timestamp", $timestamp);
          if( $timestamp > 0 )
          { $t = int( (time() - $timestamp) / (86400*7) ) } # number of weeks since last change
          
          if( $r > 0 and ($r+$t) >= $numreviewers )
          {
            push @todo, [ $lang, $package ];
          }
        }
      }
      else
      {
        push @todo, [ $lang, $package ];
      }
    }, "^$lang/packages/([\\w.+-]+)\$" );
  }
  
  DDTSS_Close($db);
  
  # Return if nothing to do
  return if scalar(@todo) == 0;

#  -t STDERR and print STDERR "Send the following: @{[ map { qq:$_->[1]($_->[0]): } @todo ]}\n";
#  return;
  
  
  for my $p (@todo)
  {
    my($lang,$package) = @$p;
    
    submit_one_package($lang,$package);
  }
}

# Process stuff in the todo area
sub process_todo
{
  my $db = DDTSS_Open_Read();

  my @todo;

  ddtss_match( $db, "todo/", sub {
    my ($key, $value) = @_;
    # $1 = type, $2 = timestamp
    next if $key !~ m,^todo/([-\w]+)/\d+\.(\d+),;

    push @todo, [$1,$key,$2];
    
    -t STDERR and print STDERR "TODO: $key\n";
  } );

  DDTSS_Close($db);

  return if scalar(@todo) == 0;

  $db = DDTSS_Open_Write();
    
  foreach my $todo (@todo)
  {
    my($type,$key,$ts) = @$todo;
    
    if( $type eq "in-log" )  # Incoming log
    { process_log($db,$key,$ts) }
    elsif( $type eq "in-data" )  # Incoming data
    { process_data($db,$key,$ts) }
    else
    { -t STDERR and print STDERR "Warning: Will not process $key\n"; }
  }

  DDTSS_Close($db);
}

# Process a log file sent by the DDTS
sub process_log
{
  my $db = shift;
  my $key = shift;
  my $data;
  return if $db->get($key,$data);  # Someone maybe did it already
  my $timestamp = shift;

  if( $data !~ /start:/ )
  {
    -t STDERR and print STDERR "Not a translation log\n";
    $db->del($key);
    return;
  }
  my ($descr) = ($data =~ /^Description: (.*)$/m);
  
  if( not defined $descr )
  {
    -t STDERR and print STDERR "Cannot find description in log\n";
    return;
  }

  my $package;
  if( $db->get("descrmatch/$descr",$package) )
  {
    -t STDERR and print STDERR "Cannot find package matching description '$descr'\n";
    return;
  }
  
  unless( $data =~ /^Description-(\w+)/m and DDTSS_Check_Valid_Language($db,$1) )
  {
     -t STDERR and print STDERR "Cannot determine language\n[$data]\n";
     return;
  }  
  
  my $lang = $1;
  
  -t STDERR and print STDERR "Moving '$key' to '$lang/logs/$timestamp/$package\n";
  
  $db->put("$lang/logs/$timestamp/$package", $data);
  $db->del($key);
}

# Process new translation sent by DDTS
sub process_data
{
  my $db = shift;
  my $key = shift;
  my $data;
  return if $db->get($key,$data);  # Someone maybe did it already
  my $timestamp = shift;

  if( $data eq "" )
  {
    -t STDERR and print STDERR "Empty value: $key\n";
    $db->del($key);
    return;
  }  
  my ($package) = ($data =~ /^# Package[^:]*: ([\w.+-]+)$/m);

  if( not defined $package )
  {
    -t STDERR and print STDERR "Cannot find package name in data\n";
    return;
  }

  my ($prio) = ($data =~ /^# Prioritize: (\d+)$/m);
  
  if( not defined $prio ) { $prio = 30 }

  my ($descr) = ($data =~ /^Description: (.*)$/m);

  if( not defined $descr )
  {
    -t STDERR and print STDERR "Cannot find description in data\n";
    return;
  }

  unless( $data =~ /^Description-(\w+)/m and DDTSS_Check_Valid_Language($db,$1) )
  {
     -t STDERR and print STDERR "Cannot determine language\n[$data]\n";
#     $db->del($key);
     return;
  }  
  
  my $lang = $1;
  
  -t STDERR and print STDERR  "Moving '$key' to '$lang/packages/$package/data'\n";
  
  {
    my $prios = "";
    $db->get("$lang/scores", $prios);
    my @prios = split /,/, $prios;
    unshift @prios, $prio;
    if( scalar(@prios) > 5 )
    { splice @prios, 5 }
    
    $db->put("$lang/scores", join(",", @prios) );
  }
  
  DDTSS_Kill_Oldest_Request($db,$lang);
  
  $db->put("$lang/packages/$package/data", $data);
  $db->put("$lang/packages/$package/age", time() );
  $db->put("$lang/packages/$package", "untranslated,$prio" );
  $db->put("descrmatch/$descr",$package);
  $db->del($key);

  DDTSS_Log( $db, "$lang/packages/$package/log", time()." processed from todo" );
}

sub DDTSS_Get_commonpackages
{
  #my $db = shift;
  my $text = shift;

  my @commonpackages=get_commonpackages_by_text($text);

  return @commonpackages;
}

1;

