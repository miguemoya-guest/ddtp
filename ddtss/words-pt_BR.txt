applet	miniaplicativo
artwork	ilustração
backend	(Sugestão: mecanismo)
binding	vínculo
bookmark	GNOME: marcador
boot	inicialização
bug tracking system	sistema de acompanhamento de bugs
builtin	embutido
built-in	embutido
character	caractere
character set	conjunto de caracteres
chat	Sugestão: bate-papo
Claws Mail mailer	Claws Mail
code folding	dobrar blocos de código
consistent	coerente
copyright	copyright
crossplatform	interplataforma
cross-platform	interplataforma
cross platform	interplataforma
Custom Debian Distribution	Distribuição Debian Personalizada
custom	personalizar
daemon	daemon
decrypt	descriptografar
delete	excluir
deprecate	tornar obsoleto
deprecated	obsoleto
desktop	área de trabalho
desktop environment	ambiente de área de trabalho
display	exibir
dock	dock, doca
download	baixar
drag'n'drop	arrastar e soltar
dropdown	suspenso(a)
dropdown list	lista suspensa
dummy package	pacote fictício ("dummy")
encrypt	criptografar
feature	recurso
feed	feed
find	localizar
for Claws Mail	para o Claws Mail
framework	infraestrutura
frontend	interface
front-end	interface
front end	interface
gathering	coleta
hardware	hardware
homepage	Página web
infrastructure	infraestrutura
more information	mais informações
multisection	multisessão
multi section	multisessão
multi-section	multisessão
multiplatform	multiplataforma
multi platform	multiplataforma
multi-platform	multiplataforma
online	on-line
on-the-fly	Sugestão: sob demanda
on the fly	Sugestão: sob demanda
parse	analisar
parser	analisador
performance	desempenho
pin	APT pinning: alfinetar
preseed	pré-configuração
pre-seed	pré-configuração
plugin	plug-in, extensão, complemento
plug-in	plug-in, extensão, complemento
popup	janela instantânea
pop-up	janela instantânea
purge	expurgar
raw	não processado
run	executar
runtime	execução
run-time	execução
run time	execução
search	pesquisar
shell	shell, camada (biologia/química)
show	mostrar
site	site
software	software
spin box	seletor numérico
standalone	autônomo
stand-alone	autônomo
stand alone	autônomo
switcher	Sugestão: alternador
syntax highlight	realce de sintaxe
syntax highlighting	realce de sintaxe
systray	bandeja de sistema
system tray	bandeja de sistema
text folding	dobrar blocos de texto
toolchain	Sugestão: cadeia base de ferramentas ("toolchain")
toolkit	kit de ferramentas
tweak	Sugestão: ajustar, refinar
tweaker	Sugestão: refinador
upstream	Sugestão: autor original
web page	página web
webpage	página web
website	site web
web site	site web
X Window System	X Window System
